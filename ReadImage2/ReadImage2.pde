  
// Example by Tom Igoe

import processing.serial.*;

int lf = 10;    // Linefeed in ASCII
String myString = null;
Serial myPort;  // The serial port

int imageSizeX = 32;
int imageSizeY = 16;

String[] imageMatrix = new String[16];
boolean isNewImage = false;

String newImageStringIndicator = "new matrix";

int matrixLineIndex = 0;

PImage newImage;

String pathToSaveImage = "D:\\savehere\\image.png";

void setup() {
  size(50, 50);
  // List all the available serial ports
  printArray(Serial.list());
  // Open the port you are using at the rate you want:
  myPort = new Serial(this, Serial.list()[3], 9600);
  myPort.clear();
  // Throw out the first reading, in case we started reading 
  // in the middle of a string from the sender.
  myString = myPort.readStringUntil(lf);
  //myString = null;
  
  newImage = createImage(imageSizeX, imageSizeY, RGB);
}



void draw() {
  while (myPort.available() > 0) {
    myString = myPort.readStringUntil(lf);
    if (myString != null) {
      //println(myString);
      isNewImage = myString.contains(newImageStringIndicator);
      
      if(isNewImage){
        matrixLineIndex = 0;
        for(int i=0; i<imageSizeY; i++){
            imageMatrix[i] = "";
        }
      }else{
        imageMatrix[matrixLineIndex] = myString;
        matrixLineIndex++;
      }
    }
    
    if(matrixLineIndex == imageSizeY){
      matrixLineIndex = 0;
      for(int i=0; i<imageSizeY; i++){
        println(imageMatrix[i]);  
      }
      //PImage newImage = createImage(imageSizeX, imageSizeY, RGB);
      newImage.loadPixels();
      println("newImage.width");
      println(newImage.width);
      println("newImage.height");
      println(newImage.height);
      for(int i=0; i<imageSizeY; i++){
        for(int j=0; j<imageSizeX; j++){
          //println(j * imageSizeY + i);
          int ledColor = int(imageMatrix[i].charAt(j));
          ledColor = ledColor % 48;
          print(ledColor);
          print(" ");
          //newImage.pixels[i * imageSizeX + j] = color(0, 153, 204, 0);
          newImage.pixels[i * imageSizeX + j] = color(0, 0, ledColor * 255, 0);
        }
        println("");
      }
      /*int dimension = newImage.width * newImage.height;
      //newImage.loadPixels();
      for (int i = 0; i < dimension; i++) { 
        println(i);
        float a = map(i, 0, newImage.pixels.length, 255, 0);
        newImage.pixels[i] = color(0, 153, 204, 0);
      }*/
      
      newImage.updatePixels();
      
      image(newImage, 0, 0);
      
      newImage.save(pathToSaveImage);
    }
  }
}
