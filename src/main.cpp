#include <Arduino.h>
#include <Ultrathin_LED_Matrix-master/LEDMatrix.h>
#include "paint.h"

DistanceCaptor dc;
AnalogJoystick aj;

// Wiring
#define CLK  8
#define OE   9
#define LAT 10
#define A   A0
#define B   A1
#define C   A2
#define D   A3
#define R    2

// Display
#define WIDTH  32 // Width of the display (px)
#define HEIGHT 16 // Height of the display (px)
#define FREQ    4 // Refresh frquency (Hz)

LEDMatrix matrix(A, B, C, D, OE, R, LAT, CLK);
uint8_t displaybuf[WIDTH * HEIGHT / 8];
uint8_t x;
uint8_t y;

uint8_t picture[HEIGHT][WIDTH];

uint8_t xCursor;
uint8_t yCursor;

uint32_t displayRefreshTimer;

void sendToProcessing()
{
  Serial.println("new matrix");

  for (uint8_t i = 0; i < HEIGHT; i++)
  {
    for (uint8_t j = 0; j < WIDTH; j++)
      Serial.print(picture[i][j]);
    
    Serial.print("\n");
  }
}

void setup()
{
  Serial.begin(9600);

  // Matrix initialization
  matrix.begin(displaybuf, WIDTH, HEIGHT);

  x = 0;
  y = 0;

  xCursor = 0;
  yCursor = 0;

  for (uint8_t i = 0; i < HEIGHT; i++)
    for (uint8_t j = 0; j < WIDTH; j++)
      picture[i][j] = 0;
}

void loop()
{
  matrix.scan();

  if ((millis() - displayRefreshTimer) > 1000 / FREQ)
  {
    displayRefreshTimer = millis();

    matrix.clear();
    for (uint8_t i = 0; i < HEIGHT; i++)
        for (uint8_t j = 0; j < WIDTH; j++)
          matrix.drawPoint(j, i, picture[i][j]);

    dc.do_loop();
    aj.do_loop();

    xCursor -= aj.directionTab[AnalogJoystick::LEFT];
    xCursor += aj.directionTab[AnalogJoystick::RIGHT];
    yCursor -= aj.directionTab[AnalogJoystick::DOWN];
    yCursor += aj.directionTab[AnalogJoystick::UP];

    if (xCursor < 0 || xCursor >= HEIGHT || yCursor < 0 || yCursor >= WIDTH)
    {
      xCursor = 0;
      yCursor = 0;

      matrix.clear();
      sendToProcessing();

      for (uint8_t i = 0; i < HEIGHT; i++)
        for (uint8_t j = 0; j < WIDTH; j++)
          picture[i][j] = 0;

      delay(1000);
    }
    else
    {
      matrix.drawPoint(yCursor, xCursor, dc.getColor() > 0 ? 1 : 0);
      picture[xCursor][yCursor] = dc.getColor() > 0 ? 1 : 0;
    }
  }
}
