#include "Arduino.h"
#include "paint.h"

DistanceCaptor::DistanceCaptor(int trigPin=13, int echoPin=12, int nbColors=16, long maxDistance=50)
{
	_trigPin=trigPin;
	_echoPin=echoPin;
	_nbColors=nbColors;
	_currentColor=0; // 0 : no color, 1..nbColors : the associated color
	_debug_color=0;
	_consecutiveOOB=0;
	_currentIndex=0;
	_maxDistance=maxDistance;
	_duration=0;
	_distance=0;
	pinMode(_trigPin,OUTPUT);
	pinMode(_echoPin,INPUT);
	for (int i = 0 ; i < _maxConsecutiveForChange ; i++)
		_measuredColor[i] = 0;
}

void DistanceCaptor::do_loop()
{
	digitalWrite(_trigPin, LOW);  // Added this line
	delayMicroseconds(2); // Added this line
	digitalWrite(_trigPin, HIGH);
	delayMicroseconds(10); // Added this line
	digitalWrite(_trigPin, LOW);
	_duration = pulseIn(_echoPin, HIGH);
	_distance = (_duration/2) / 29.1;
	if (_distance <= _maxDistance)
	{
		for (int i = 1; i <= _nbColors ; i++)
		{
			if ((i-1)*_maxDistance/_nbColors < _distance && _distance <= i*_maxDistance/_nbColors)
			{
				_measuredColor[_currentIndex] = i;
				_currentIndex ++;
				_currentIndex = _currentIndex % _maxConsecutiveForChange;
				_consecutiveOOB = 0;
			}
		}
	}
	else 
		_consecutiveOOB ++;
	if (_consecutiveOOB >= _maxConsecutiveForChange)
		_currentColor = 0;
	bool are_same = true;
	for (int i = 1 ; i < _maxConsecutiveForChange ; i++)
	{
		if (_measuredColor[i] != _measuredColor[0])
		{
			are_same = false;
			break;
		}
	}
	if (are_same)
		_currentColor = _measuredColor[0];
	if (_currentColor != _debug_color)
	{
		Serial.println(_currentColor);
		_debug_color = _currentColor;
	}
	//delay(100);
}

int DistanceCaptor::getColor()
{
	return _currentColor;
}


AnalogJoystick::AnalogJoystick(int pinX = A4, int pinY = A5, int threshold = 250)
{
	_pinX = pinX;
	_pinY = pinY;
	_threshold = threshold;
	pinMode(_pinX, INPUT);
	pinMode(_pinY, INPUT);
	//_baseX = analogRead(_pinX);
	//_baseY = analogRead(_pinY);
	_baseX = 500;
	_baseY = 500;
	for (int i = 0; i < 4 ; i++)
		directionTab[i] = 0;
}

void AnalogJoystick::do_loop()
{
	int x=analogRead(_pinX);
	int y=analogRead(_pinY);
	directionTab[LEFT] = x < _baseX-_threshold ? 1 : 0;
	directionTab[RIGHT] = x > _baseX+_threshold ? 1 : 0;
	directionTab[UP] = y < _baseY-_threshold ? 1 : 0;
	directionTab[DOWN] = y > _baseY+_threshold ? 1 : 0;
}
