class DistanceCaptor {
	private:
	int _trigPin;
	int _echoPin;
	int _nbColors;
	int _currentColor; // 0 : no color, 1..nbColors : the associated color
	int _debug_color;
	int _consecutiveOOB;
	int _currentIndex;
	long _maxDistance;
	long _duration, _distance;
	static const int _maxConsecutiveForChange = 5;
	int _measuredColor[_maxConsecutiveForChange];
	public:
	DistanceCaptor(int trigPin=13, int echoPin=12, int nbColors=16, long maxDistance=50);
	void do_loop();
	int getColor();
};

class AnalogJoystick
{
	private:
	int _pinX = A4;
	int _pinY = A5;

	int _baseX;
	int _baseY;

	int _threshold = 250;

	public:
	enum {LEFT,RIGHT,UP,DOWN};
	AnalogJoystick(int pinX = A4, int pinY = A5, int threshold = 250);
	void do_loop();
	int directionTab[4];
};
