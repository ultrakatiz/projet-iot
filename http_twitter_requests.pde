Twitter twitter;

void authenticate() {
  ConfigurationBuilder cb = new ConfigurationBuilder();  
 
  cb.setOAuthConsumerKey("****");
  cb.setOAuthConsumerSecret("****");
  cb.setOAuthAccessToken("****");
  cb.setOAuthAccessTokenSecret("****"); 
  twitter = new TwitterFactory(cb.build()).getInstance();
}

void tweet_image(String path) {
  
  File file = new File(path);  
  tweet_image(file);
}

void tweet_image(File file) {
    StatusUpdate status = new StatusUpdate("");
    status.setMedia(file); // set the image to be uploaded here.
    try {
      twitter.updateStatus(status);
     
    }catch(Exception e) { println(e.getMessage());}
}
