# IOT project

This is a project done in IOT class using an Arduino, some electrical components and the stuff we learned.

## Objective

The goal was to make an application using Arduino, Processing, an internet connection, and some components. We chose to develop a painting application on a minimalist screen, that could send the picture on a twitter account.

## Work achieved

We used a led screen, a distance captor and a joystick. The Arduino manages those devices in order to create a picture, which is then sent to Processing using the Serial library. The image is then sent on twitter using the API.
